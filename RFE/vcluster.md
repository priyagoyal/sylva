# Multitenancy on CAAS using Vcluster

Vcluster is used for creating and managing virtual Kubernetes clusters. A virtual cluster lives inside a namespace on a host cluster, but it appears to the user as if it’s a full-blown, standalone Kubernetes cluster
With vCluster the tenants will feel that they own the Kubernetes cluster, and they will have almost all the admin privileges inside their vcluster, without changing the privileges in the underlying cluster or without compromising the underlying cluster.

### Why virtual kubernetes clusters
- Cost Efficient
- Lightweight
- Strict Isolation
- cluster wide permissions

### Vcluster components
- Control Plane
- API Server
- Controller Manager
- Storage Backend
- Syncer

### Vcluster Architecture

<img src="Vcluster.png"/>


# Technical implementation

Vcluster is an open-source project of Loft Labs

### Prerequisites
- Kubernetes 1.18+
- Helm 3+

Vcluster components can be installed with the help of Vcluster helm chart  from repo https://charts.loft.sh

Install Vcluster CLI to connect to installed Vcluster
```
https://github.com/loft-sh/vcluster/releases/latest/download/vcluster-darwin-amd64
```

# How to test it

To confirm that Vcluster CLI is successfully installed
```
vcluster --version
```

Connect to installed Vcluster
```
vcluster connect [RELEASE_NAME] -n [RELEASE_NAMESPACE]
```